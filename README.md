# PhD

You can find the web version of the PhD text as a pdf-file under `text.pdf` in the main folder.

## Additional figures for Chapter 3

Chapter 3 only shows 3 measurement results of participant of the measurement campaign at the university hospital UZ Brussel in Jette. The complete set of figures of the measurement results can be found in the folder `fig`.
